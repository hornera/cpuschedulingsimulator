#CS365 Operating System  
##Project: CPU Scheduling  

In this project, you will program a cpu simulator. You need to  
	1. Implementation of a CPU scheduler simulation to compare two schedules described in Chapter 5 (use any programming language that you like); and  
	2. Create a 1-2 page report describing your evaluation of these different scheduling algorithms. Did one work better then the other? Which algorithm might be better then another for a given purpose?  
  
###Program description  
A job can be defined by an arrival time and a burst time. For example, here’s a sequence of jobs:  
  
<0, 100>, <2, 55>, <2, 45>, <5, 10>…  
  
The first job arrives at time 0 and requires 100ms of CPU time to complete; the second job arrives at time 2 and requires 55ms of CPU time; the third job arrives at time 2 and requires 45ms; and so on. You can assume that time is divided into millisecond units.  
  
Your simulator should first generate a sequence of jobs. The burst lengths can be determined by selecting a random number from an exponential distribution.   
  
There should also be a minimum job length of 2ms, so that the total burst duration for a job is 2ms plus the value selected from the exponential distribution (which should be between 0 and 40). So the shortest job will require for 2ms of CPU time and the longest, 42ms.  
  
Your program should simulate the arrival of jobs for up to n milliseconds and then stop.  
  
Once the jobs have been generated, you will need to compare the performance of different scheduling algorithms on the same set of jobs. You can write one program that runs both algorithms or write two separate programs.  
  
For each scheduling algorithm, your program should measure at least (1) the CPU utilization, (2) the average job throughput per second, and (3) the average job turnaround time.
